<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'basus' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '#fTdYY!rt+?!]A]y]OT3A=]1{s+HWk$-$U}6mOYuU0p|#By_c|?|}+M*.!ZSzTut' );
define( 'SECURE_AUTH_KEY',  'dLGU18f=od[TipbbcnBll~M`Ua7;;S(e4w0AIAJ.O}8&ZIn$IJ`v_.A3 2{g/bTV' );
define( 'LOGGED_IN_KEY',    'Ll@SQfHAEtHN9a9i-*n~ioNe-t5i>W7&GsrXG#AN3:WeMc&IWQ4NWeiV3i@sP~<$' );
define( 'NONCE_KEY',        'o^^vYS>HXgQ8)mHrEWn8:lvrr9:k,P[[d3$An^ztfOHAcwc2p>ka0>S|A+5#k^Fn' );
define( 'AUTH_SALT',        'qI(:/a5ji6?E{2oi7=?*si#/|T[|_DNgnP:n*z9T)I(]!tyjR,+Q%D-`37h(2*9X' );
define( 'SECURE_AUTH_SALT', 'CV&9:&^$aI)@*9qPz(:f+w^H56w6WA_X>ZB$3C.Vk}`ESECEYPKEOw-V}0=f(lmD' );
define( 'LOGGED_IN_SALT',   'J4^A2,GGvo 1lI^uP@Hwr4mMIPb^9W7L,B,6-LWmOF!W%qwlo:v?28NSoI{t:iUe' );
define( 'NONCE_SALT',       ':vzr5c*XXRu;Wb/CH{8:Y&~S8d jw2Fz? yG6U@{LV#8kG/1QCN>WQk.kFbds@*f' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG', false);
define('WP_DEBUG_DISPLAY', false);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
