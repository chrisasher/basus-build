<?php defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
/*
Plugin Name: Oink Social
Plugin URI:  http://wordpress.com
Description: Easy custom social links shortcode for developers
Version:     1.0
Author:      Chris Asher
Author URI:  http://oinkdigital.com.au
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/
function oink_social_share_shortcode() {
	$title = 'Share:';
	// the url
	$this_url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	/* share buttons */
	$out = '<div class="social-links-holder">';

	$out .= '<h3>'.$title.'</h3>';

	//facebook
	$out .= '<a title="send to Facebook"
  href="http://www.facebook.com/sharer.php?s=100&p[title]=' . urlencode(get_the_title()) . '&p[summary]=' . urlencode(get_the_excerpt()) . '&p[url]='.urlencode($this_url).'&p[images][0]='.urlencode(get_the_post_thumbnail_url()).'"
  class="facebook-popup btn-link social-sharing-link"><i class="fab fa-facebook-f" aria-hidden="true"></i>
</a>';

	//twitter
	$out .= '<a class="twitter-popup btn-link social-sharing-link" href="http://twitter.com/share"><i class="fab fa-twitter" aria-hidden="true"></i>
</a>';

	// pintrest
// 	$out .= '<a href="http://pinterest.com/pin/create/button/?url='.urlencode($this_url).'&media='.urlencode(get_the_post_thumbnail_url()).'&description=' . urlencode(get_the_excerpt()) . '" class="pintrest-popup btn-link social-sharing-link"><i class="fab fa-pinterest-p" aria-hidden="true"></i>
// </a>';

	//tumblr
	// $out .= '<a href="https://www.tumblr.com/widgets/share/tool?canonicalUrl='.$this_url.'" class="tumblr-popup btn-link social-sharing-link" target="_blank"><i class="fab fa-tumblr" aria-hidden="true"></i></a>';

	//linkedin
	$out .= '<a href="https://www.linkedin.com/cws/share?url='.$this_url.'" class="linkedin-popup btn-link social-sharing-link" target="_blank"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a>';

	//email link
	$out .= '<a href="mailto:?subject=This%20article%20might%20interest%20you&body='.urlencode($this_url).'" class="email btn-link social-sharing-link"><i class="far fa-envelope" aria-hidden="true"></i>
</a>';


	$out .= '</div>';

	echo $out;
}
add_shortcode( 'share-buttons', 'oink_social_share_shortcode' );


add_action( 'wp_enqueue_scripts', 'load_oink_social' );
function load_oink_social(){
	wp_enqueue_script('oink-social',plugins_url('js/oink-social.js',__FILE__),array('jquery'));
}
