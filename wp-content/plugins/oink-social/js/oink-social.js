jQuery(document).ready(function($){
	// twitter popup
	$('.twitter-popup').click(function(event) {
		event.preventDefault();
		socialPopup($,this,'Twitter', 575, 400);
	});

	// tumblr popup
	$('.tumblr-popup').click(function(event) {
		event.preventDefault();
		socialPopup($,this,'Twitter', 540, 600);
	});

	// Facebook popup
	$('.facebook-popup').click(function(event) {
		event.preventDefault();
		socialPopup($,this,'Facebook', 540, 600);
	});

	// pintrest popup
	$('.pintrest-popup').click(function(event) {
		event.preventDefault();
		socialPopup($,this,'Pintrest', 540, 600);
	});

	// google plus popup
	$('.google-popup').click(function(event) {
		event.preventDefault();
		socialPopup($,this,'Google', 540, 600);
	});

	// linkedIn popup
	$('.linkedin-popup').click(function(event) {
		event.preventDefault();
		socialPopup($,this,'LinkedIn', 540, 600);
	});
})


function socialPopup($,target,name,width,height){
	var left   = ($(window).width()  - width)  / 2,
		top    = ($(window).height() - height) / 2,
		url    = target.href,
		opts   = 'status=1' +
			',width='  + width  +
			',height=' + height +
			',top='    + top    +
			',left='   + left;

	window.open(url, name, opts);

	return false;
}
