<?php
get_header();

$image = get_the_post_thumbnail_url();
$title = get_the_title();
//		$content = get_the_content();
$theme = get_field('page_class');
$id = get_the_ID();
$intro_text = get_field('intro_text');

$background = ($image)? "style='background: url(\"$image\");'" : '';
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
$bodyclass = get_body_class();
$backgroundColor = get_field('background_color');
$heroDisable = get_field('disable_header_image');
 ?>

<?php if(!$heroDisable): ?>
<div class="height-control">
	<div class="header-hero-standard parallax rollover" data-paroller-factor="0.3" data-paroller-type="background" style="background: url('<?php echo $image[0] ?>')">
		<div class="grid grid-middle grid-center">
			<div class="col-10">
					<h1><?php echo $title ?></h1>
			</div>
		</div>
	</div>		
</div>
<?php endif; ?>

<?php

echo '<main>';

/* Start the Loop */
while ( have_posts() ) :
	the_post();
	get_template_part( 'partials/content-page' );

endwhile; // End of the loop.

echo '</main>';

get_footer();