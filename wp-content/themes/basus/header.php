<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); 
$facebook = get_theme_mod( 'facebook_link' );
$instagram = get_theme_mod( 'instagram_link' );
$link = get_theme_mod('projects_link'); ?>

<div id="mobile-logo" class="mob-logo">
    <a class="logo-link" href="<?php echo get_home_url() ?>"><img class="site-logo col-6" src="<?php if (get_theme_mod( 'logo_image' )) : echo get_theme_mod('logo_image'); else: echo get_template_directory_uri().'/images/basus_logo.svg'; endif; ?>" alt=""></a>
</div>
<div id="mobile-cta" class="mob-cta">
    <a class="button" target="_blank" href="<?php echo $trial ?>">Project Enquiries</a>
</div>

<header id="header" class="desktop">
    <div class="grid toolbar-holder">
        <div class="col-2 logo-holder">
            <a href="<?php echo home_url() ?>">
                <img src="<?php echo get_template_directory_uri() ?>/images/basus_logo.png" alt="">
            </a>
        </div>
        <div class="col menu-holder" data-push-left="off-1" data-push-right="off-1">
            <nav>
                <?php
                wp_nav_menu(array('theme_location' => 'primary'));
                ?>
            </nav>
        </div>
        <div class="col-1 socials" data-push-left="off-0">
            <a target="_blank" href="<?php echo $instagram ?>">
                <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M11.5 23C17.8513 23 23 17.8513 23 11.5C23 5.14873 17.8513 0 11.5 0C5.14873 0 0 5.14873 0 11.5C0 17.8513 5.14873 23 11.5 23Z" fill="black"/>
                    <path d="M15.1231 7.16779C14.6505 7.16779 14.3354 7.48285 14.3354 7.95546C14.3354 8.42806 14.7293 8.74313 15.1231 8.74313C15.5957 8.74313 15.9108 8.42806 15.9108 7.95546C15.9108 7.48285 15.517 7.16779 15.1231 7.16779Z" fill="white"/>
                    <path d="M11.5787 8.19177C9.76708 8.19177 8.27051 9.68835 8.27051 11.5C8.27051 13.3116 9.76708 14.8082 11.5787 14.8082C13.3904 14.8082 14.8869 13.3116 14.8869 11.5C14.8869 9.68835 13.3904 8.19177 11.5787 8.19177ZM11.5787 13.6267C10.3972 13.6267 9.45201 12.6815 9.45201 11.5C9.45201 10.3185 10.3972 9.37328 11.5787 9.37328C12.7602 9.37328 13.7054 10.3185 13.7054 11.5C13.7054 12.6815 12.7602 13.6267 11.5787 13.6267Z" fill="white"/>
                    <path d="M14.1781 18.274H8.82196C6.61649 18.274 4.72607 16.4623 4.72607 14.1781V8.8219C4.72607 6.53766 6.61649 4.72601 8.82196 4.72601H14.1781C16.3836 4.72601 18.1953 6.53766 18.1953 8.74314V14.1781C18.274 16.4623 16.4624 18.274 14.1781 18.274ZM8.82196 5.98629C7.24662 5.98629 6.06512 7.24656 6.06512 8.74314V14.1781C6.06512 15.7534 7.32539 16.9349 8.82196 16.9349H14.1781C15.7535 16.9349 16.935 15.6746 16.935 14.1781V8.8219C16.935 7.24656 15.6747 6.06505 14.1781 6.06505H8.82196V5.98629Z" fill="white"/>
                </svg>
            </a>
            <a target="_blank" href="<?php echo $facebook ?>">
            <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M23 11.5395C23 5.13746 17.8801 0 11.5 0C5.11986 0 0 5.13746 0 11.5395C0 17.3093 4.17466 22.1306 9.68836 23V14.8591H6.77397V11.5395H9.68836V9.01031C9.68836 6.08591 11.4212 4.50515 14.0205 4.50515C15.2808 4.50515 16.6199 4.74227 16.6199 4.74227V7.58763H15.2021C13.7842 7.58763 13.3116 8.45704 13.3116 9.4055V11.5395H16.5411L16.0685 14.8591H13.3904V22.921C18.8253 22.1306 23 17.3093 23 11.5395Z" fill="black"/>
            </svg>
            </a>
        </div>
    </div>  
</header>

<div class="cta-holder">
    <a href="<?php echo $link ?>" class="cta-header">Project Enquiries</a>
</div>

<div id="mobile-menu" class="mobile hidden">
    <?php wp_nav_menu(array('theme_location' => 'primary')); ?>
</div>