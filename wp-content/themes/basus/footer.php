<?php

$logo_1 = get_theme_mod('footer_logo_1');
$logo_2 = get_theme_mod('footer_logo_2');
$logo_3 = get_theme_mod('footer_logo_3');

?>
<section id="footer" class="container">
    <div class="grid">
        <div class="col-6">
            <img src="<?php echo get_template_directory_uri() ?>/images/basus_logo.png" alt="">
            <div class="flex">
                <img src="<?php echo $logo_1 ?>" alt="">
                <img src="<?php echo $logo_2 ?>" alt="">
                <img src="<?php echo $logo_3 ?>" alt="">
            </div>
        </div>
        <div class="col-3 location">
            <?php if(is_registered_sidebar('footer-2')){
                dynamic_sidebar('footer-2');
            } ?>
        </div>
        <div class="col-3 links">
            <?php if(is_registered_sidebar('footer-3')){
                dynamic_sidebar('footer-3');
            } ?>
        </div>
        <div class="copyright">
            <p>&copy; Copyright <?php echo date('Y') ?> Basus Build | Website by <a target="_blank" href="https://oinkdigital.com.au">OINK Digital</a></p>
        </div>
    </div>
<?php wp_footer(); ?>
</body>
</html>
