<?php 

$image = get_field('image');
$background = get_field('background_color');
$imageAlignment = get_field('image_alignment');
$textArea = get_field('text_area');
$openBlock = get_field('open_block');
$enableButton = get_field('enable_button');
$link = get_field('link');  
$linkText = get_field('link_text');  

if($imageAlignment == 'right') {
    $class = 'grid';
    $position = 'right';
    $mobposition = 'right-mob';
} else {
    $class = 'grid grid-reverse image-left';
    $position = 'left';
    $mobposition = 'left-mob';
}

?>

<section class="adjustable-block <?php echo $background ?> <?php echo $mobposition ?>">
    <div class="<?php echo $class ?>">
        <div class="col-6_md-8_sm-12 text-section">
            <?php echo $textArea ?>
            <?php if($enableButton): ?>
                <div class="cta-button">
                    <a href="<?php echo $link ?>" class="button"><?php echo $linkText ?></a>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-5_md-8_sm-12" data-push-left="off-1_md-0">
            <div class="image-overlay">
                <div class="the-image <?php echo $position ?>" style="background-image: url(<?php echo $image ?>)"></div>
            </div>
        </div>
    </div>
</section>