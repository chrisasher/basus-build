<!-- custompost type here -->
<!-- replace the repeater with post loop -->
<?php

global $post;

$args = array(  
    'post_type' => 'testimonials',
    'post_status' => 'publish',
    'posts_per_page' => -1, 
    'orderby' => 'title', 
    'order' => 'ASC',
);

$loop = new WP_Query( $args ); ?> 

<div class="testimonials">
    <div class="heading">
        <h3>Testimonials</h3>
    </div>
    <div class="grid first-col grid-middle">
        <div class="col-5">
            <div class="image-fixed slider-for">
                <?php if ($loop->have_posts()): ?>
                    <?php while($loop->have_posts()): ?>
                        <?php $loop->the_post();
                        $image = get_the_post_thumbnail_url( $post->ID ) ?>
                        <div class="the-image" style="background-image: url(<?php echo $image ?>)"></div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-6 slider-nav">
            <?php if ($loop->have_posts()): ?>
                <?php while($loop->have_posts()): ?>
                    <?php $loop->the_post(); ?>
                        <div class="testimonial-details">
                            <img class="first-quote" src="<?php echo get_template_directory_uri() ?>/images/quote-in.png" alt="">
                            <span class="author-name"><?php the_title(); ?></span>
                            <?php the_excerpt(); ?>
                            <img class="second-quote" src="<?php echo get_template_directory_uri() ?>/images/quote-out.png" alt="">
                        </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</div>