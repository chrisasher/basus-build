<?php
/**
 * Restricted Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create class attribute allowing for custom "className" and "align" values.
?>


<section class="gutenberg-block">
    <div class="grid">
        <div class="col-12">
            <InnerBlocks />
        </div>
    </div>
</section>
