<?php $link = get_theme_mod('projects_link'); ?>

<div class="cta bg-2">
    <div class="grid grid-middle grid-justifyBetween">
        <div class="col-8_sm-12">
            <p>Are you looking to start a similar project? Enquire now !</p>
        </div>
        <div class="col-2_sm-12" data-push-left="off-2_sm-0">
            <a class="white-button" href="<?php echo $link ?>">Project Enquiries</a>
        </div>
    </div>
</div>