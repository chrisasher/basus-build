/**
 * Created by chris on 12/04/2016.
 */
jQuery(document).ready(function($){

	$(document).on('scroll', function(){
		let headerPosition = $(document).scrollTop();
		if(headerPosition > 100) {
			$('#header').addClass('scrolled');
			$('.cta-holder').addClass('scrolled');
		} else {
			$('#header').removeClass('scrolled');
			$('.cta-holder').removeClass('scrolled');
		}
	})

	$('#mobile-menu').slicknav({
		allowParentLinks: true,
		parentTag: 'a'
	});

	// testimonial slider
	$('.slider-for').slick({
		centerPadding: 0,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		variableHeight: true,
		fade: true,
		asNavFor: '.slider-nav'
	  });
	  $('.slider-nav').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		asNavFor: '.slider-for',
		dots: false,
		centerMode: false,
		fade: true,
		autoplay: true,
		autoplaySpeed: 20000,
		arrows: false,
		focusOnSelect: true
	  });

	  $('.parallax').paroller();

})

jQuery(window).load(function($){

})

jQuery(window).resize(function($){

})

function sizetorow(divheight, target, adjustment){
	var $ = jQuery;
	if($(window).width() > 800){
		var maxHeight = 0;

		$(target).css('min-height',maxHeight);
		var maxHeight = $(divheight).height() - adjustment;
		console.log(maxHeight);
		//$(target).each(function(){
		//
		//})
		setTimeout(function(){
			$(target).css('min-height',maxHeight);
		},600)
	}
}

function makesquare(target){
	var $ = jQuery;
	var width = $(target).width();
	$(target).height(width);
}