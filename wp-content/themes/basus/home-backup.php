<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<?php if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="page-title screen-reader-text projects-header"><?php single_post_title(); ?></h1>
				</header>
			<?php endif; ?>

			<div class="grid projects">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php 
						$link = get_the_permalink();
						$img = (get_the_post_thumbnail_url())? get_the_post_thumbnail_url(null,'large') : get_home_url().'/wp2016/wp-content/uploads/2016/05/Blog-Post.jpg';
						$id = get_the_ID();
						$background = ($img)? "style='background: url(\"$img\");'" : '';
						$author = get_the_author();
						$format = get_post_format();
						$term = get_queried_object();
						$excerpt = get_the_excerpt($id);
						$title = get_the_title($id);
					?>

					<div class="col-4_md-8_sm-12 single-post">
						<div class="holder">
							<a href="<?php echo $link; ?>">
								<div class="background wp-bg-img" <?php echo $background ?>></div>
							</a>
						</div>
						<div class="blog-details">
							<h4><a href="<?php echo $link; ?>"><?php echo $title ?></a></h4>
							<p><?php echo wp_trim_words($excerpt, 20, '...') ?></p>
						</div>
						<a class="more-button" href="<?php echo $link; ?>">More Info</a>
					</div>

				<?php endwhile; ?>
			
			</div>

        <?php $default_posts_per_page = get_option( 'posts_per_page' ); ?>
		<!-- // are we on page one? -->
		<?php if($default_posts_per_page < 7): ?>
			<div class="pagination">
    			<?php post_pagination(); ?>
			</div>
		<?php endif; ?>

		<?php else : ?>
			<p>No Posts found!</p>
		<?php endif; ?>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
