<?php
get_header(); 

// Start the loop.
if(have_posts()){
	while ( have_posts() ){
		the_post();
		$image = get_the_post_thumbnail_url();
		$title = get_the_title();
//		$content = get_the_content();
		$theme = get_field('page_class');
        $id = get_the_ID();
		$heroText = get_field('hero_title');

        $background = ($image[0])? "style='background: url(\"$image\");'" : '';
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
		$bodyclass = get_body_class();
		$button = get_field('hero_button');
		?>

        <div class="fullscreen wp-bg-img home-hero" <?php echo $background; ?>>

			<!-- replace with custom field -->
			<div class="grid grid-middle">
				<div class="col-7_md-8_sm-12 show-bg">
					<h1><?php echo $heroText ?></h1>
					<?php if( $button ):
						$link_url = $button['url'];
						$link_title = $button['title'];
						$link_target = $button['target'] ? $button['target'] : '_self';
					?>
					<a class="block-button button <?php echo $buttonClass ?>" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
					<?php endif; ?>
				</div>
			</div>

		</div>
		
		<main>
		
        <?php the_content() ?>
        
        </main>
		<?php
		// End of the loop.
	}
}
?>

<?php
get_footer();