<?php
function theme_customize_register( $wp_customize ) {
    
    // Site settings
	$wp_customize->add_section( 'theme_settings' , array(
		'title'      => __( 'Footer Logos', 'theme' ),
		'priority'   => 30,
	) );
	// Footer Logo
	// Footer Logo 1
	$wp_customize->add_setting('footer_logo_1', array(
	));
    $wp_customize->add_control( new WP_Customize_Image_Control(
        $wp_customize, 'footer_logo_1', array(
            'label' => __('Footer Logo 1', 'theme'),
            'section'   => 'theme_settings',
            'settings'  => 'footer_logo_1',
        )
	));
    // Footer Logo 2
    $wp_customize->add_setting('footer_logo_2', array(
    ));
    $wp_customize->add_control( new WP_Customize_Image_Control(
        $wp_customize, 'footer_logo_2', array(
            'label' => __('Footer Logo 2', 'theme'),
            'section'   => 'theme_settings',
            'settings'  => 'footer_logo_2',
        )
    ));
    // Footer Logo 3
    $wp_customize->add_setting('footer_logo_3', array(
    ));
    $wp_customize->add_control( new WP_Customize_Image_Control(
        $wp_customize, 'footer_logo_3', array(
            'label' => __('Footer Logo 3', 'theme'),
            'section'   => 'theme_settings',
            'settings'  => 'footer_logo_3',
        )
    ));

	/*
	 * social links start
	 */
	$wp_customize->add_section( 'theme_social_links' , array(
		'title'      => __( 'Social Links', 'theme' ),
		'priority'   => 30,
	) );
	// facbook
	$wp_customize->add_setting( 'facebook_link' , array(
		'default' => 'http://',
	) );
	//twitter
	$wp_customize->add_setting( 'twitter_link' , array(
		'default' => 'http://',
	) );
	// instagram
	$wp_customize->add_setting( 'instagram_link' , array(
		'default' => 'http://',
	) );

	$wp_customize->add_control(
		'facebook_link', array(
			'label' => __('Facebook Link', 'theme'),
			'section' => 'theme_social_links',
			'settings' => 'facebook_link',
			'type' => 'url',
		)
	);
	$wp_customize->add_control(
		'twitter_link', array(
			'label' => __('Twitter Link', 'theme'),
			'section' => 'theme_social_links',
			'settings' => 'twitter_link',
			'type' => 'url',
		)
	);
	$wp_customize->add_control(
		'instagram_link', array(
			'label' => __('Instagram Link', 'theme'),
			'section' => 'theme_social_links',
			'settings' => 'instagram_link',
			'type' => 'url',
		)
	);
	/*
	 * social links end
	 */

	/*
	 * Address Info Start
	 */
	$wp_customize->add_section( 'theme_contact_info' , array(
		'title'      => __( 'Contact Info', 'theme' ),
		'priority'   => 30,
	) );


	// Office street address
	$wp_customize->add_setting( 'place' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'place', array(
			'label' => __('Place Name', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'place',
			'type' => 'text',
		)
	);

	// Office street address
	$wp_customize->add_setting( 'projects_link' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'projects_link', array(
			'label' => __('Project Enquiries Link', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'projects_link',
			'type' => 'url',
		)
	);

	// Contact Phone
	$wp_customize->add_setting( 'contact_phone' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'contact_phone', array(
			'label' => __('Contact Phone', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'contact_phone',
			'type' => 'text',
		)
	);

	// Contact Email
	$wp_customize->add_setting( 'contact_email' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'contact_email', array(
			'label' => __('Contact Email', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'contact_email',
			'type' => 'text',
		)
	);

}
add_action( 'customize_register', 'theme_customize_register' );

function wpdc_add_custom_gutenberg_color_palette() {
	add_theme_support(
		'editor-color-palette',
		[
			[
				'name'  => esc_html__( 'Charcoal', 'wpdc' ),
				'slug'  => 'bg-1',
				'color' => '#3A3939',
			],
			[
				'name'  => esc_html__( 'white', 'wpdc' ),
				'slug'  => 'bg',
				'color' => '#ffffff',
			],
		]
	);
}
add_action( 'after_setup_theme', 'wpdc_add_custom_gutenberg_color_palette' );

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
		'page_title' 	=> 'Theme CTA Settings',
		'menu_title'	=> 'CTA Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
	));
	
}