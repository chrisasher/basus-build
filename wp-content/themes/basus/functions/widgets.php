<?php
/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Twenty Sixteen 1.0
 */
function theme_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Footer 1', 'theme' ),
        'id'            => 'footer-1',
        'description'   => __( 'Appears on footer', 'theme' ),
//        'before_widget' => '<div id="%1$s" class="widget %2$s">',
//        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widgettitle how-to-book-3">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Footer 2', 'theme' ),
        'id'            => 'footer-2',
        'description'   => __( 'Appears on footer', 'theme' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<strong class="widgettitle how-to-book-3">',
        'after_title'   => '</strong>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Footer 3', 'theme' ),
        'id'            => 'footer-3',
        'description'   => __( 'Appears on footer', 'theme' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<strong class="widgettitle how-to-book-3">',
        'after_title'   => '</strong>',
    ) );


}
add_action( 'widgets_init', 'theme_widgets_init' );