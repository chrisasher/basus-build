<?php
// Blocks

function oink_theme_setup() {

    // Add Align Wide and Full Width options to blocks.
    // This is done by adding the corresponding classname to the block’s wrapper.
    add_theme_support( 'align-wide' );

    // Stop users picking their own colours in the block editor
    add_theme_support( 'disable-custom-colors' );

    // Stop users from changing font sizes
    add_theme_support('disable-custom-font-sizes');

    // Custom Editor Colours
    add_theme_support( 'editor-color-palette', array(
        array(
            'name' => __( 'Black', 'oink' ),
            'slug' => 'black',
            'color' => '#000000',
        ),
        array(
            'name' => __( 'White', 'oink' ),
            'slug' => 'white',
            'color' => '#ffffff',
        ),
        array(
            'name' => __( 'Grey', 'oink' ),
            'slug' => 'grey',
            'color' => '#f2f2f2',
        ),
    ) );

    // Add a stylesheet for editing in the Dashboard
    add_theme_support( 'editor-styles' ); // if you don't add this line, your stylesheet won't be added
    add_editor_style( 'dashboard.css' );
}
add_action( 'after_setup_theme', 'oink_theme_setup' );

//Remove Gutenberg Styling from the frontend
function wps_deregister_styles() {
    wp_dequeue_style( 'wp-block-library' );
}
add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );

// Add Reusable blocks to menu
function linked_url() {
    add_menu_page( 'linked_url', 'Reusable Blocks', 'read', 'edit.php?post_type=wp_block', '', 'dashicons-editor-table', 22 );
}
add_action( 'admin_menu', 'linked_url' );

function linkedurl_function() {
    global $menu;
    $menu[1][2] = "/wp-admin/edit.php?post_type=wp_block";
}
add_action( 'admin_menu' , 'linkedurl_function' );

// Register ACF Blocks
function register_acf_block_types() {

    acf_register_block_type(array(
        'name'              => 'Adjustable Block',
        'title'             => __('Adjustable Block'),
        'description'       => __('An adjustable block.'),
        'render_template'   => 'partials/blocks/adjustable-block.php',
        'category'          => 'formatting',
        'icon'              => 'dashicons-layout',
        'keywords'          => array( 'content', 'adjustable' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Testimonials Block',
        'title'             => __('Testimonials Block'),
        'description'       => __('Testimonials slider block.'),
        'render_template'   => 'partials/blocks/testimonials.php',
        'category'          => 'formatting',
        'icon'              => 'dashicons-layout',
        'keywords'          => array( 'content', 'adjustable' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Contact Block',
        'title'             => __('Contact Block'),
        'description'       => __('Contact block.'),
        'render_template'   => 'partials/blocks/full-width.php',
        'category'          => 'formatting',
        'icon'              => 'dashicons-layout',
        'keywords'          => array( 'content', 'adjustable' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Map Block',
        'title'             => __('Map Block'),
        'description'       => __('Map block.'),
        'render_template'   => 'partials/blocks/map.php',
        'category'          => 'formatting',
        'icon'              => 'dashicons-layout',
        'keywords'          => array( 'content', 'adjustable' ),
    ));
}

// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'register_acf_block_types');
}