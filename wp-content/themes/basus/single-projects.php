<?php
get_header();

$image = get_the_post_thumbnail_url();
$title = get_the_title();
//		$content = get_the_content();
$theme = get_field('page_class');
$id = get_the_ID();
$intro_text = get_field('intro_text');

$background = ($image)? "style='background: url(\"$image\");'" : '';
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
$bodyclass = get_body_class();
$backgroundColor = get_field('background_color');
 ?>

<div class="height-control">
	<div class="header-hero-standard parallax rollover" data-paroller-factor="0.3" data-paroller-type="background" style="background: url('<?php echo $image[0] ?>')">
		<div class="grid grid-middle grid-center">
			<div class="col-10">
					<h1><?php echo $title ?></h1>
			</div>
		</div>
	</div>		
</div>

<div class="grid">
	<div class="col-5_sm-12 back-link">
		<a href="<?php echo get_post_type_archive_link('projects'); ?>"><i class="fas fa-chevron-left"></i>Back to Projects</a>
	</div>
</div>

<?php
echo '<main>';
// Start the loop.
if(have_posts()){
	while ( have_posts() ){
		the_post();

		$image = (get_the_post_thumbnail_url())? get_the_post_thumbnail_url() : get_stylesheet_directory_uri().'/imgs/image.jpg';
		$title = get_the_title();
		$id = get_the_ID();
		$bodyclass = get_body_class();

		?>
		<?php get_template_part( 'partials/content-page' ); ?>
		<?php
		// End of the loop.
	}
}

echo '</main>';

?>

<div class="grid">
	<div class="col-5_sm-12 socials">
		<?php echo do_shortcode('[share-buttons]'); ?>
	</div>
</div>

<?php get_template_part( 'partials/content-cta' ); ?>


<?php
get_footer();